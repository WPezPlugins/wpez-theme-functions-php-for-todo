## WPezClasses: Images Size Names Choose

__Extends the ImageSizeRegister array standard and does the 'image_size_names_choose' filter The ezWay.__

Adds to this select: The Content > Add Media > Pick an Image > Attachment Display Settings > (Select) Size.
   
> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### Simple Example

_Recommended: Use the WPezClasses autoloader (link below)._

```

use WPezSuite\WPezClasses\ImageSizeNamesChoose\ClassImageSizeNamesChoose as ISNC;
use WPezSuite\WPezClasses\ImageSizeNamesChoose\ClassHooks as Hooks;

$arr_imgs = [
    'size1' => [
        'name' => 'sqr_md',
        'width' => '500',
        'height' => '500'
        ],
    'size2' => [
        'name' => 'sqr_lg',
        'width' => '650',
        'height' => '650'
        'names_choose' => [
            'active' => false,
            ]
        ]
    ];
    
$arr_img = [
        'name' => 'sqr_xl',
        'width' => '800',
        'height' => '800'
        'crop' => true
        'names_choose' => [
            'option' => 'Sqr XL'
            'content_width_compare_override' => true
        ]
    ];
    
$new_isr = new ISNC();
// load via array
$new_isr->loadImages($arr_imgs);
// or push one at a time
$new_isr->pushImage($arr_img);
    
$new_hooks = new Hooks($new_isr);
$new_hooks->register();

```

Note: The Content Width setting can (automatically) factor in (or not) into which image sizes you wish to add.


### FAQ

__1) Why?__

Traditional WordPress "decouples" this from add_image_size(). But the ezWay unites the image definitions for size args and 'image_size_names_choose' into a simple single array. 

__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 


 __3) - I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 


### HELPFUL LINKS

 - https://gitlab.com/wpezsuite/WPezClasses/ImageSizeRegister

 - https://developer.wordpress.org/reference/hooks/image_size_names_choose/
 
 - https://codex.wordpress.org/Plugin_API/Filter_Reference/image_size_names_choose
 
 - https://codex.wordpress.org/Content_Width
 

### TODO

n/a

### CHANGE LOG

- v0.0.3 - Monday 22 April 2019
    - UPDATED: interface file / name

- v0.0.2 - Saturday 20 April 2019
    - ADDED: ability to remove names from the names choose array

- v0.0.1 - Wednesday 17 April 2019
  - Hey! Ho!! Let's go!!!