<?php

/**
 * WordPress add_theme_support() done The ezWay.
 *
 * Instead of manually coding line after line of add_theme_support()s , now you simply configure an array and pass that
 * to this class / methods.
 *
 *
 */


namespace WPezThemeFunctionsPhpForTODO\App\Core\ThemeAddSupport;

// No WP? Die! Now!!
if (!defined('ABSPATH')) {
    header('HTTP/1.0 403 Forbidden');
    die();
}


class ClassThemeAddSupport implements InterfaceThemeAddSupport
{

    protected $_arr_features;
    protected $_arr_ret;
    protected $_arr_ret_ats;
    protected $_arr_defaults;
    protected $_arr_feat_args;

    public function __construct()
    {
        $this->setPropertyDefaults();
    }

    protected function setPropertyDefaults()
    {

        $this->_arr_features = [];
        $this->_arr_ret = [];
        $this->_arr_ret_ats = [];

        $this->_arr_defaults = [
            'active' => true,
            'feature' => false,
            'args' => false,
        ];

        // supported features and the args they allow / require
        $this->_arr_feat_args = [

            'post-formats' => ['arr', 'none'],
            'post-thumbnails' => ['arr', 'none'],
            'custom-background' => ['arr', 'none'],
            'custom-header' => ['arr', 'none'],
            'custom-header-upload' => ['none'],
            'custom-logo' => ['arr', 'none'],
            'automatic-feed-links' => ['none'],
            'html5' => ['arr'],
            'title-tag'  => ['none'],
            'customize-selective-refresh-widgets' => ['none'],
            'starter-content' => ['arr'],
            'wp-block-styles' => ['none'],
            'align-wide' => ['none'],
            'editor-styles'  => ['none'],
            'editor-font-sizes' => ['arr'],
            'editor-color-palette' => ['arr'],
            'responsive-embeds' => ['none'],

            'dark-editor-style' => ['none'],
            'disable-custom-font-sizes' => ['none'],
            'disable-custom-colors' => ['none'],
        ];

    }

    /**
     * https://developer.wordpress.org/reference/functions/add_theme_support/
     *
     * @param bool $str_feature
     * @param bool $args
     * @return bool
     */
    public function add($str_feature = false, $args = false){

        if ( ! is_string($str_feature)){
            return false;
        }
        return $this->pushFeature([
            'feature' => $str_feature,
            'args' => $args
        ]);
    }

    /**
     * https://developer.wordpress.org/reference/functions/add_theme_support/#post-formats
     *
     * @param bool $arr
     * @return bool
     */
    public function addPostFormats( $arr = false){

        $str = 'post-formats';
        if (is_array($arr)){
            return $this->addMaster($str, $arr);
        }
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/reference/functions/add_theme_support/#post-thumbnails
     *
     * @param bool $arr
     * @return bool
     */
    public function addPostThumbnails( $arr = false){

        $str = 'post-thumbnails';
        if (is_array($arr)){
            return $this->addMaster($str, $arr);
        }
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/reference/functions/add_theme_support/#custom-background
     *
     * @param bool $arr
     * @return bool
     */
    public function addCustomBackground ( $arr = false){

        $str = 'custom-background';
        if (is_array($arr)){
            return $this->addMaster($str, $arr);
        }
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/reference/functions/add_theme_support/#custom-header
     *
     * @param bool $arr
     * @return bool
     */
    public function addCustomHeader( $arr = false){

        $str = 'custom-header';
        if (is_array($arr)){
            return $this->addMaster($str, $arr);
        }
        return $this->addMaster( $str );
    }

    /**
     * @return bool
     */
    public function addCustomHeaderUpload(){

        $str = 'custom-header-upload';
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/reference/functions/add_theme_support/#custom-logo
     *
     * @param bool $arr
     * @return bool
     */
    public function addCustomLogo( $arr = false){

        $str = 'custom-logo';
        if (is_array($arr)){
            return $this->addMaster($str, $arr);
        }
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/reference/functions/add_theme_support/#feed-links
     *
     * @return bool
     */
    public function addAutomaticFeedLinks(){

        $str = 'automatic-feed-links';
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     *
     * @param bool $arr
     * @return bool
     */
    public function addHTML5( $arr = false){

        $str = 'html5';
        if (is_array($arr)){
            return $this->addMaster($str, $arr);
        }
        return false;
    }

    /**
     * https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     *
     * @return bool
     */
    public function addTitleTag(){

        $str = 'title-tag';
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/reference/functions/add_theme_support/#customize-selective-refresh-widgets
     *
     * @return bool
     */
    public function addCustomizeSelectiveRefreshWidgets(){

        $str = 'customize-selective-refresh-widgets';
        return $this->addMaster( $str );
    }

    /**
     * https://roots.io/using-and-customizing-wordpress-starter-content/
     *
     * @param bool $arr
     * @return bool
     */
    public function addStarterContent( $arr = false){

        $str = 'starter-content';
        if (is_array($arr)){
            return $this->addMaster($str, $arr);
        }
        return false;
    }

    /**
     * https://developer.wordpress.org/block-editor/developers/themes/theme-support/
     *
     * @return bool
     */
    public function addWPBlockStyles(){

        $str = 'wp-block-styles';
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/block-editor/developers/themes/theme-support/
     *
     * @return bool
     */
    public function addAlignWide(){

        $str = 'align-wide';
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/block-editor/developers/themes/theme-support/
     *
     * @return bool
     */
    public function addEditorStyles(){

        $str = 'editor-styles';
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/block-editor/developers/themes/theme-support/
     *
     * @param bool $arr
     * @return bool
     */
    public function addEditorFontSizes( $arr = false){

        $str = 'editor-font-sizes';
        if (is_array($arr)){
            return $this->addMaster($str, $arr);
        }
        return false;
    }

    /**
     * https://developer.wordpress.org/block-editor/developers/themes/theme-support/
     *
     * @param bool $arr
     * @return bool
     */
    public function addEditorColorPalette( $arr = false){

        $str = 'editor-color-palette';
        if (is_array($arr)){
            return $this->addMaster($str, $arr);
        }
        return false;
    }

    /**
     * https://developer.wordpress.org/block-editor/developers/themes/theme-support/
     *
     * @return bool
     */
    public function addResponsiveEmbeds(){

        $str = 'responsive-embeds';
        return $this->addMaster( $str );
    }


    /**
     * https://developer.wordpress.org/block-editor/developers/themes/theme-support/
     *
     * @return bool
     */
    public function addDarkEditorStyle (){

        $str = 'dark-editor-style';
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/block-editor/developers/themes/theme-support/
     *
     * @return bool
     */
    public function addDisableCustomFontSizes(){

        $str = 'disable-custom-font-sizes';
        return $this->addMaster( $str );
    }

    /**
     * https://developer.wordpress.org/block-editor/developers/themes/theme-support/
     *
     * @return bool
     */
    public function addDisableCustomColors(){

        $str = 'disable-custom-colors';
        return $this->addMaster( $str );
    }

    /**
     * @param bool $str_feat
     * @param bool $mix
     * @return bool
     */
    protected function addMaster($str_feat = false, $mix = false ){

        $arr = ['feature' => $str_feat];
        if ( is_array($mix) ){
            return $this->pushFeature( array_merge( $mix, $arr));
        }
        return $this->pushFeature( $arr);

    }

    public function pushFeature($arr = false)
    {

        if (!is_array($arr)) {
            return false;
        }
        $arr_temp = array_merge($this->_arr_defaults, $arr);

        if (!is_string($arr_temp['feature'])) {
            return false;
        }
        $arr_temp['feature'] = trim(strtolower($arr_temp['feature']));

        $this->_arr_features[] = $arr_temp;

    }


    public function loadFeatures($arr_feats = false)
    {

        if (!is_array($arr_feats)) {
            return false;
        }

        $this->_arr_ret = [];
        foreach ($arr_feats as $key => $arr_feat) {

            $this->_arr_ret[$key] = $this->pushFeature($arr_feat);
        }

        return $this->_arr_ret;


    }

    public function getFeatures()
    {

        return $this->_arr_features;
    }


    public function addThemeSupport()
    {

        foreach ($this->_arr_features as $arr_feat) {


            // active?
            if ($arr_feat['active'] !== true) {
                $this->_arr_ret_ats [$arr_feat['feature']] = 'active = false';
                continue;
            }

            // feature supported
            if ( ! isset ($this->_arr_feat_args[$arr_feat['feature']]) && is_array($this->_arr_feat_args[$arr_feat['feature']] ) ){
                $this->_arr_ret_ats[$arr_feat['feature']] = 'feature is invalid or not supported';
                continue;

            }

            // args can be array?
            if (  is_array($this->_arr_feat_args[$arr_feat['feature']]) && in_array('arr', $this->_arr_feat_args[$arr_feat['feature']]) && is_array($arr_feat['args'])) {

                $this->_arr_ret_ats[$arr_feat['feature']] = add_theme_support($arr_feat['feature'], $arr_feat['args']);
                continue;
            }

            // args can be none?
            if (  is_array($this->_arr_feat_args[$arr_feat['feature']]) && in_array('none', $this->_arr_feat_args[$arr_feat['feature']]) ) {

                $this->_arr_ret_ats[$arr_feat['feature']] = add_theme_support($arr_feat['feature']);
                continue;
            }

            // something went wrong
            $this->_arr_ret_ats[$arr_feat['feature']] = 'This feature requires an array of args.';


        }
        return $this->_arr_ret_ats;
    }

    public function getAddThemeSupport()
    {

        return $this->_arr_ret_ats;
    }

}
