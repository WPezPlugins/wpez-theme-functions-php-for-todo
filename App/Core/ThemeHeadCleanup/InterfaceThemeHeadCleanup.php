<?php

namespace WPezThemeFunctionsPhpForTODO\App\Core\ThemeHeadCleanup;

interface InterfaceThemeHeadCleanup {

	public function cleanupHead();

	public function filterEmojiSvgUrl();

	public function filterTinyMMCEPlugins( $plugins );

	public function filterStyleLoaderSrc( $str_src );


}