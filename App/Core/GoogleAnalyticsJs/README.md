## WPezClasses: Google Analytics Js

__Add the Google Analytics analytics.js code to WordPress and do it The ezWay.__

Imagine a lite-weight plugin without the (unnecessary) UI. 

https://developers.google.com/analytics/devguides/collection/analyticsjs/


> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

_Recommended: Use the WPezClasses autoloader (link below)._

A basic example.

```
public function addClassAnalyticsJs(){ 
   
   $new_ga = new ClassAnalyticsJs();
   $new_ga->setUA('UA-XXXXX-Y');
   
   // if the user has any of the capabilites listed the snipped will not be rendered.
   // Note: Use capabilities, not roles.
   
   $new_ga->setCurrentUserCanNoRender([
           'switch_themes' => true
       ]);

   $str_ga = $new_ga->getGATrackingSnippet();
   echo $str_ga;
}

add_action('wp_head', [$this,'addClassAnalyticsJs' ])

```

### FAQ

__1) Why?__

I wanted something as simple and light weight as possible. No need for a UI, when I'm only going to set it (i.e., the UA, etc.) and forget it.

__2) Can I use this in my plugin or theme?__

Yes, but to be safe, please change the namespace. 

 __3) I'm not a developer, can we hire you?__
 
Yes, that's always a possibility. If I'm available, and there's a good mutual fit.   

  

### HELPFUL LINKS

- https://gitlab.com/wpezsuite/WPezClasses/WPezAutoload

- __Roles and Capabilities__
  
  https://codex.wordpress.org/Roles_and_Capabilities 
 
- __Function Reference / current user can__

   https://codex.wordpress.org/Function_Reference/current_user_can

 
 

### TODO

- Various setters
- composer.js
- Add additional settings / functionality parms, as covered in the GA analytics.js docs.
- Remove / move method: CheckFiletype()  
- Custom trackers


### CHANGE LOG

- v0.0.0
    - Work in progress / "experimental"