<?php

namespace WPezSuite\WPezClasses\GoogleAnalyticsJs;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.0 403 Forbidden' );
    die();
}

if ( ! class_exists( 'ClassGoogleAnalyticsJs' ) ) {
    class ClassGoogleAnalyticsJs {

        protected $_str_ua;
        // TODO custom trackers
        // https://developers.google.com/analytics/devguides/collection/analyticsjs/creating-trackers
        // https://developers.google.com/analytics/devguides/collection/analyticsjs/command-queue-reference#create
        // protected $_arr_tracker_names;
        protected $_bool_render_async;
        protected $_arr_current_user_can_no_render;

        // https://support.google.com/analytics/answer/2444872?hl=en
        protected $_bool_enable_remarketing;

        protected $_str_render_msg_header;
        protected $_arr_render_msg;
        protected $_bool_render_msg;
        protected $_str_render_msg;

        protected $_str_script_tag_attrs;
        protected $_str_ga_js;
        protected $_bool_ga_debug;
        protected $_str_ga_js_debug;

        // https://developers.google.com/analytics/devguides/collection/gajs/methods/gaJSApi_gat?csw=1#_gat._anonymizeIp
        // TODO protected $_bool_anonymize_ip;

        // https://developers.google.com/analytics/devguides/collection/analyticsjs/enhanced-link-attribution
        protected $_bool_enhanced_link_attribution;
        protected $_str_ela_cookie_name;
        protected $_int_ela_duration;
        protected $_int_levels;

        protected $_str_ga_tracking_snippet;

        public function __construct() {

            $this->setPropertyDefaults();

        }

        protected function setPropertyDefaults() {

            $this->_str_ua                         = false;
            $this->_arr_tracker_names              = [
                // 'name1' => true,
            ];
            $this->_bool_render_async              = false;
            $this->_arr_current_user_can_no_render = [];

            // TODO +set
            $this->_bool_enable_remarketing = false;

            // TODO +set
            $this->_str_render_msg_header = 'TODO - https://gitlab.com...';
            // TODO +set
            $this->_arr_render_msg = [
                'ua_empty'              => 'ua empty',
                'ua_bad'                => 'ua invalid',
                'current_user_can_true' => 'current_user_can true',
                'debug_true'            => 'debug === true'
            ];
            // TODO +set
            $this->_bool_render_msg = true;
            // TODO +set
            $this->_str_render_msg       = '';
            $this->_str_script_tag_attrs = 'data-cfasync="false"';
            $this->_str_ga_js            = 'https://www.google-analytics.com/analytics.js';
            $this->_str_ga_js_debug      = 'https://www.google-analytics.com/analytics_debug.js';
            $this->_bool_ga_debug        = false;

            // TODO $this->_bool_anonymize_ip = false;

            // TODO + sets
            $this->_bool_enhanced_link_attribution = false;
            $this->_str_ela_cookie_name            = '_gali';
            $this->_int_ela_duration               = 30;
            $this->_int_levels                     = 3;

            $this->_str_ga_tracking_snippet = '';
        }

        protected function setString( $str = false, $default ) {

            if ( is_string( $str ) ) {
                return $str;
            }

            return $default;
        }


        public function setUA( $str_ua = false ) {

            $this->_str_ua = $this->setString( $str_ua, $this->_str_ua );
        }

        public function setGtagJs( $bool = true ) {

            if ( is_bool( $bool ) ) {
                $this->_bool_gtag_js = $bool;

                return true;
            }

            return false;
        }

        public function setRenderAsync( $bool ) {

            if ( is_bool( $bool ) ) {
                $this->_bool_render_async = $bool;

                return true;
            }

            return false;
        }


        public function setCurrentUserCanNoRender( $arr = [] ) {

            if ( is_array( $arr ) ) {
                $this->_arr_current_user_can_no_render = $arr;
            }
        }

        public function setScriptTagAttrs( $str = '' ) {

            // TODO - come up with some safe / escape'able
            //$this->_str_script_tag_attrs = $str;
        }

        public function getGATrackingSnippet() {

            $str_ret = "<!-- ++++++++++ " . esc_html( $this->_str_render_msg_header ) . " ++++++++++ -->";

            // track user?
            if ( $this->renderSnippet() === true ) {

                $str_ga_js = $this->gaJs();
                $str_ret   = "<script type='text/javascript' " . $this->_str_script_tag_attrs . ">";

                if ( $this->_bool_render_async !== true ) {

                    $str_ret .= "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){";
                    $str_ret .= "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),";
                    $str_ret .= "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)";
                    $str_ret .= "})(window,document,'script','" . $str_ga_js . "','ga');";

                    $str_ret .= $this->gaDebug();
                    $str_ret .= "ga('create', '" . esc_js( $this->_str_ua ) . "', 'auto')";
                    $str_ret .= $this->enhancedLinkAttribution();
                    $str_ret .= "ga('send', 'pageview')";

                } else {

                    $str_ret .= "window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;";
                    $str_ret .= $this->gaDebug();
                    $str_ret .= "ga('create', '" . esc_js( $this->_str_ua ) . "', 'auto');";
                    $str_ret .= $this->enhancedLinkAttribution();
                    $str_ret .= "ga('send', 'pageview');";
                    $str_ret .= "</script>";
                    $str_ret .= "<script async src='" . $str_ga_js . "' " . esc_attr( $this->_str_script_tag_attrs ) . "></script>";
                }

                $str_ret .= $this->enableRemarketing();

                $str_ret .= "</script>";

            } else {
                if ( $this->_bool_render_msg !== false ) {
                    $str_ret .= '<!--' . esc_html( $this->_str_render_msg ) . '-->';
                }
            }

            return $str_ret;
        }

        protected function renderSnippet() {

            if ( empty( $this->_str_ua ) ) {

                $this->_str_render_msg = $this->_arr_render_msg['ua_empty'];

                return false;
            }

            if ( ! $this->regexUA( $this->_str_ua ) ) {

                $this->_str_render_msg = $this->_arr_render_msg['ua_bad'];

                return false;
            }

            $bool_render = true;

            $bool_render = $this->evaluateCurrentUserCan();

            if ( $bool_render === false && $this->_bool_ga_debug === true ) {
                $this->_str_render_msg = $this->_arr_render_msg['debug_true'];
                $bool_render           = true;
            }

            return $bool_render;
        }


        protected function evaluateCurrentUserCan() {

            $bool_ret = true;
            foreach ( $this->_arr_current_user_can_no_render as $str_capability => $bool_active ) {

                if ( $bool_active === true ) {

                    if ( current_user_can( $str_capability ) ) {
                        $this->_str_render_msg = $this->_arr_render_msg['current_user_can_true'];
                        $bool_ret              = false;
                        break;
                    }
                }
            }

            return $bool_ret;
        }

        protected function gaJs() {

            $str_ret = $this->_str_ga_js;
            if ( $this->_bool_ga_debug === true ) {

                $str_ret               = $this->_str_ga_js_debug;
                $this->_str_render_msg = $this->_arr_render_msg['debug_true'];
            }

            return $str_ret;
        }

        protected function regexUA( $str ) {

            // https://gist.github.com/faisalman/924970
            return preg_match( '/^ua-\d{4,9}-\d{1,4}$/i', strval( $str ) ) ? true : false;
        }

        protected function gaDebug() {

            if ( $this->_bool_ga_debug === true ) {
                return "window.ga_debug = {trace: true};";
            }

            return '';

        }

        protected function enhancedLinkAttribution() {

            if ( $this->_bool_enhanced_link_attribution === true ) {

                $str_ret = '';
                $str_ret .= "ga('require', 'linkid', {";
                $str_ret .= "'cookieName': '" . esc_js( $this->_str_ela_cookie_name ) . "',";
                $str_ret .= "'duration': " . esc_js( $this->_int_ela_duration ) . ",";
                $str_ret .= "'levels': " . esc_js( $this->_int_levels );
                $str_ret .= "});";

                return $str_ret;
            }

            return '';
        }

        protected function enableRemarketing() {

            if ( $this->_bool_enable_remarketing === true ) {
                return "ga('require', 'displayfeatures');";
            }

            return '';

        }


        /**
         * __if__ (eventually) the url for the js  can be set, then it will need to be checked.
         *  https://www.wordfence.com/learn/how-to-prevent-file-upload-vulnerabilities/
         *
         * @param $str_path
         */
        protected function checkFiletype( $str_path ) {

            // https://codex.wordpress.org/Function_Reference/get_allowed_mime_types
            $arr_allowed_mimes = [
                'js' => 'application/javascript'
            ];

            $arr_wpcf = wp_check_filetype( basename( $str_path ), $arr_allowed_mimes );

            if ( ! empty( $arr_wpcf['type'] ) ) {

                return true;
            }

            return false;
        }
    }
}