<?php

namespace WPezThemeFunctionsPhpForTODO\App\Core\ImageSizeRegister;

// No WP? Die! Now!!
if ( ! defined( 'ABSPATH' ) ) {
	header( 'HTTP/1.0 403 Forbidden' );
	die();
}

class ClassImageSizeRegister implements InterfaceImageSizeRegister {

	protected $_arr_imgs;
	protected $_arr_defaults;
	protected $_arr_ret;

	public function __construct() {

		$this->setPropertyDefaults();

	}

	protected function setPropertyDefaults() {

		$this->_arr_imgs     = [];
		$this->_arr_defaults = [
			'active' => true,
			'name'   => false,
			'width'  => 0,
			'height' => 0,
			'crop'   => false,
			'thumbnail' => false
		];
		$this->_arr_ret      = [];

	}


	public function updateDefaults($arr_defaults = false ){

		if ( is_array($arr_defaults)){

			$this->_arr_defaults = array_merge($this->_arr_defaults, $arr_defaults);

		}

	}

	public function pushImage( $arr_args = false ) {

		if ( is_array( $arr_args ) ) {

			$this->_arr_imgs[] = $arr_args;

			return true;
		}

		return false;
	}


	public function loadImages( $arr_images = false ) {

		if ( is_array( $arr_images ) ) {
			$arr_ret = [];
			foreach ( $arr_images as $str_ndx => $arr_args ) {

				$arr_ret[ $str_ndx ] = $this->pushImage( $arr_args );

			}

			return $arr_ret;
		}

		return false;
	}


	public function registerImageSizes() {

		foreach ( $this->_arr_imgs as $str_ndx => $arr_args ) {

			if ( ! is_array($arr_args) ){
				continue;
			}

			$arr_args = array_merge( $this->_arr_defaults, $arr_args );
			if ( $arr_args['active'] === false ) {
				continue;
			}

			if ( $arr_args['thumbnail'] === true ) {

				// https://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
				// "Note: To enable featured images, the current theme must include add_theme_support( 'post-thumbnails' )";
				set_post_thumbnail_size( $arr_args['width'], $arr_args['height'], $arr_args['crop'] );
			} else {
				// TODO - should add_image_size() be here?
			}

			$this->_arr_ret[ $str_ndx ] = add_image_size( $arr_args['name'], $arr_args['width'], $arr_args['height'], $arr_args['crop'] );
		}

	}

	public function getReturn() {

		return $this->_arr_ret;
	}
}