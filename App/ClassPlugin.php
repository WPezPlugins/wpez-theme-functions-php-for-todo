<?php
/*
Plugin Name: WPezPlugins: Theme Customize: TODO
Plugin URI: https://gitlab.com/WPezPlugins/wpez-theme-customize
Description: Boilerplate for customizations of your theme / parent theme.
Version: 0.0.2
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: TODO
*/

namespace WPezThemeFunctionsPhpForTODO\App;

use WPezThemeFunctionsPhpForTODO\App\Core\HooksRegister\ClassHooksRegister;

use WPezThemeFunctionsPhpForTODO\App\Core\ThemeHeadCleanup\ClassThemeHeadCleanup as THC;
use WPezThemeFunctionsPhpForTODO\App\Core\ThemeHeadCleanup\ClassHooks as THChooks;

use WPezThemeFunctionsPhpForTODO\App\Core\ThemeAddSupport\ClassThemeAddSupport as TAS;
use WPezThemeFunctionsPhpForTODO\App\Core\ThemeAddSupport\ClassHooks as TAShooks;

use WPezThemeFunctionsPhpForTODO\App\Core\ImageSizeRegister\ClassImageSizeRegister as ISR;
use WPezThemeFunctionsPhpForTODO\App\Core\ImageSizeRegister\ClassHooks as ISRhooks;

use WPezThemeFunctionsPhpForTODO\App\Core\ImageSizeNamesChoose\ClassImageSizeNamesChoose as ISNC;
use WPezThemeFunctionsPhpForTODO\App\Core\ImageSizeNamesChoose\ClassHooks as ISNChooks;


class ClassPlugin
{

    // protected $_arr_thc;
    protected $_arr_tas;
    protected $_arr_isr;

    protected $_new_hooks_reg;
    protected $_arr_actions;
    protected $_arr_filters;
    protected $_new_wpcore;


    public function __construct()
    {

        $this->setPropertyDefaults();

        $this->themeHeadCleanup();

        $this->themeAddSupport();

        $this->imageSizeRegister();

        $this->imageSizeNamesChoose();

        $this->actions(false);

        $this->filters(false);

        $this->other(false);

        $this->wpCore(false);

        // this should be last
        $this->hooksRegister();

    }

    protected function setPropertyDefaults()
    {
        // $this->_arr_thc = [];

        $this->_arr_tas = [

            'f1' => [
                'active' => false,
                'feature' => 'post-formats',
                'args' => []
            ],
            'f2' => [
                'active' => true,
                'feature' => 'post-thumbnails',
                'args' => ['post', 'page']
            ],
            'f3' => [
                'active' => true,
                'feature' => 'editor-color-palette',
                'args' => [
                    [
                        'name' => 'Blue - #2D3A66',
                        'slug' => 'primary',
                        'color' => '#2D3A66',
                    ],
                    [
                        'name' => 'Grey - #A4A9BC',
                        'slug' => 'secondary',
                        'color' => '#A4A9BC',
                    ],
                    [
                        'name' => 'Mint - #77E1E5',
                        'slug' => 'accent-dark',
                        'color' => '#77E1E5',
                    ],
                    [
                        'name' => 'Off-White - #F9F2F3',
                        'slug' => 'accent-light',
                        'color' => '#F9F2F3',
                    ],
                    [
                        'name' => 'White - #ffffff',
                        'slug' => 'white',
                        'color' => '#ffffff',
                    ],
                    [
                        'name' => 'Black - #111111',
                        'slug' => 'black',
                        'color' => '#111111',
                    ],
                ]
            ],
            'f4' => [
                'active' => true,
                'feature' => 'responsive-embeds',
                'args' => []
            ],
            'f5' => [
                'active' => true,
                'feature' => 'disable-custom-font-sizes',
                'args' => []
            ],
            'f6' => [
                'active' => true,
                'feature' => 'disable-custom-colors',
                'args' => []
            ],
        ];

        $this->_arr_isr = [
            'size1' => [
                'name' => 'wpez_xs',
                'width' => 576,
                'height' => 9999,
                'names_choose' => [
                    'option' => 'XS - 576w',
                    'content_width_compare_override' => true
                ]
            ],
            'size2' => [
                'name' => 'wpez_sm',
                'width' => 768,
                'height' => 9999,
                'names_choose' => [
                    'option' => 'SM - 768w',
                    'content_width_compare_override' => true
                ]
            ],
            'size3' => [
                'name' => 'wpez_md',
                'width' => 992,
                'height' => 9999,
                'names_choose' => [
                    'option' => 'MD - 992w',
                    'content_width_compare_override' => true
                ]
            ],
            'size4' => [
                'name' => 'wpez_lg',
                'width' => 1200,
                'height' => 9999,
                'names_choose' => [
                    'option' => 'LG - 1200w',
                    'content_width_compare_override' => true
                ]
            ],
            'size5' => [
                'name' => 'wpez_xl',
                'width' => 1500,
                'height' => 9999,
                'names_choose' => [
                    'option' => 'XL - 1500w',
                    'content_width_compare_override' => true
                ]
            ]
        ];

        $this->_new_hooks_reg = new ClassHooksRegister();
        $this->_arr_actions = [];
        $this->_arr_filters = [];

        // $this->_new_wpcore = new ClassWPCore();
    }


    protected function themeHeadCleanup($bool = true)
    {
        if ($bool !== true) {
            return;
        }

        // https://gitlab.com/wpezsuite/WPezClasses/ClassThemeHeadCleanup
        $new = new THC();
        $hooks = new THChooks($new);
        $hooks->register();
    }

    protected function themeAddSupport($bool = true)
    {
        if ($bool !== true) {
            return;
        }

        $new = new TAS();
        $new->loadFeatures($this->_arr_tas);
        $hooks = new TAShooks($new);
        $hooks->register();
    }

    protected function imageSizeRegister($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $new = new ISR();
        $new->loadImages( $this->_arr_isr);
        $hooks = new ISRhooks($new);
        $hooks->register();
    }

    protected function imageSizeNamesChoose($bool = true){

        $new = new ISNC();
        $new->loadImages($this->_arr_isr);
        $new->setAddWhere('before');
        $hooks = new ISNChooks($new);
        $hooks->register();
    }

    /**
     * After gathering (below) the arr_actions and arr_filter, it's time to
     * make some RegisterHook magic
     */
    protected function hooksRegister()
    {

        $this->_new_hooks_reg->loadActions($this->_arr_actions);

        $this->_new_hooks_reg->loadFilters($this->_arr_filters);

        $this->_new_hooks_reg->actionRegister();

    }


    public function actions($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $new_actions = new ClassActions();

        $this->_arr_actions[] = [
            'active' => true, // active does not have to be set. it - in ClassRegisterHooks - is default: true
            'hook' => 'suki/frontend/logo',
            'component' => $new_actions,
            'callback' => 'removeActionSukiFrontendLogo',
            'priority' => 5
        ];

        $this->_arr_actions[] = [
            'active' => true, // active does not have to be set. it - in ClassRegisterHooks - is default: true
            'hook' => 'suki/frontend/logo',
            'component' => $new_actions,
            'callback' => 'customSukiDefaultLogo',
            'priority' => 15
        ];

        $this->_arr_actions[] = [
            'active' => true,
            'hook' => 'suki/frontend/mobile_logo',
            'component' => $new_actions,
            'callback' => 'removeActionSukiFrontendMobileLogo',
            'priority' => 5
        ];

        $this->_arr_actions[] = [
            'active' => true,
            'hook' => 'suki/frontend/mobile_logo',
            'component' => $new_actions,
            'callback' => 'customSukiDefaultMobileLogo',
            'priority' => 15
        ];

        $this->_arr_actions[] = [
            'active' => true,
            'hook' => 'suki/frontend/after_header',
            'component' => $new_actions,
            'callback' => 'sukiFrontendAfterHeader',
            'priority' => 15
        ];

        $this->_arr_actions[] = [
            'active' => true,
            'hook' => 'suki/frontend/entry_grid/header',
            'component' => $new_actions,
            'callback' => 'removeActionSukiFrontendEntryGridHeader',
            'priority' => 5
        ];

        $this->_arr_actions[] = [
            'active' => FALSE,
            'hook' => 'suki/frontend/entry/before_header',
            'component' => $new_actions,
            'callback' => 'customSukiFrontendEntryBeforeHeaderWrapperOpen',
            'priority' => 5
        ];

        $this->_arr_actions[] = [
            'active' => FALSE,
            'hook' => 'suki/frontend/entry/after_header',
            'component' => $new_actions,
            'callback' => 'customSukiFrontendEntryBeforeHeaderWrapperClose',
            'priority' => 5
        ];


        $this->_arr_actions[] = [
            'active' => true,
            'hook' => 'suki/frontend/before_main',
            'component' => $new_actions,
            'callback' => 'customSukiFrontendBeforeMainScrollAddBodyClass',
            'priority' => 5
        ];

        $this->_arr_actions[] = [
            'active' => true,
            'hook' => 'suki/frontend/after_main',
            'component' => $new_actions,
            'callback' => 'removeActionSukiSinglePostNavigation',
            'priority' => 5
        ];

        $this->_arr_actions[] = [
            'active' => true,
            'hook' => 'suki/frontend/after_main',
            'component' => $new_actions,
            'callback' => 'sukiSinglePostNavigation',
            'priority' => 15
        ];

    }

    public function filters($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $new_filters = new ClassFilters();

        $this->_arr_filters[] = [
            'hook' => 'suki/frontend/entry/thumbnail_classes',
            'component' => $new_filters,
            'callback' => 'frontendEntryThumbnailClasses',
            // 'priority' => 75
        ];
    }


    public function other($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $new_other = new ClassOther();
    }


    public function wpCore($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $this->wpDequeueStyle(false);

        $this->wpDequeueScript(false);

        $this->deregisterPostType(false);

        $this->postTypeSupport(false);

        $this->unregisterSidebar(false);

        $this->unregisterWidget(false);

        $this->removeSetting(false);
    }


    protected function wpDequeueStyle($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'style-name-1' => true, // true = remove
            // 'style-name-2' => false // false = leave
        ];
        $this->_new_wpcore->setStyles($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'wp_enqueue_scripts',
            'component' => $this->_new_wpcore,
            'callback' => 'wpDequeueStyle',
            'priority' => 100
        ];
    }


    protected function wpDequeueScript($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'script-name-1' => true,
            // 'script-name-2' => false
        ];
        $this->_new_wpcore->setScripts($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'wp_enqueue_scripts',
            'component' => $this->_new_wpcore,
            'callback' => 'wpDequeueScript',
            'priority' => 100
        ];
    }


    protected function deregisterPostType($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'post-type-1' => true,
            // 'post-type-2' => false
        ];
        $this->_new_wpcore->setPostTypes($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'init',
            'component' => $this->_new_wpcore,
            'callback' => 'deregisterPostType',
            'priority' => 100
        ];
    }


    protected function postTypeSupport($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'support-1' => true,
            // 'support-2' => false
        ];
        $this->_new_wpcore->setPostTypesSupport($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'init',
            'component' => $this->_new_wpcore,
            'callback' => 'removePostTypeSupport',
            'priority' => 100
        ];

        $this->_arr_actions[] = [
            'hook' => 'init',
            'component' => $this->_new_wpcore,
            'callback' => 'addPostTypeSupport',
            'priority' => 105
        ];
    }


    protected function unregisterSidebar($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'sidebar-name-1' => true,
            // 'sidebar-name-2' => false
        ];
        $this->_new_wpcore->setSidebars($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'init',
            'component' => $this->_new_wpcore,
            'callback' => 'unregisterSidebar',
            'priority' => 100
        ];

    }


    protected function unregisterWidget($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        $arr_args = [
            // 'widget-name-1' => true,
            // 'widget-name-2' => false
        ];
        $this->_new_wpcore->setWidgets($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'init',
            'component' => $this->_new_wpcore,
            'callback' => 'unregisterWidget',
            'priority' => 100
        ];

    }

    protected function removeSetting($bool = true)
    {

        if ($bool !== true) {
            return;
        }

        // Don't forget to use set_theme_mod() to "replace" the values for the settings you remove
        $arr_args = [
            // 'setting-name-1' => true,
            // 'setting-name-2' => false
        ];
        $this->_new_wpcore->setSettings($arr_args);

        $this->_arr_actions[] = [
            'hook' => 'customize_register',
            'component' => $this->_new_wpcore,
            'callback' => 'removeSetting',
            'priority' => 100
        ];

    }
}