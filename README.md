## WPezThemeFunctionsPhp

__Typical theme functions.php stuff decoupled into a free-standing plugin (so it's persistent and portable).__

> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --

### OVERVIEW

Settings and such that are more brand-centric (and tend to transcend activated theme) belong here. For example, add_theme_support()'s editor-color-palette.

This separation also allows you to keep your theme focused on presentation, and mitigate the mix of general presentation and brand specific needs.    


> --
>
> IMPORTANT
>
> This is the _boilerplate_. That is, you're going to have to write or modify some code. 
>
> -- 


__Please Note__

Be sure to update the namespace in each of the files. WPezThemeFunctionsPhpForTODO should be search & replaced with your own.  

Note: Technically, this isn't necessary if you're only going to have a single instance of the plugin activated at any one time.
  
That said, more than one means the plugin's folder will have to be changed / customized for each version beyond the initial one.

Pardon me if this is all stating the obvious.

### GETTING STARTED

Naturally, this leans on appropriate WPezClasses. With those in place, ClassPlugin.php focuses on the settings. The ezWay: more configuring, less coding. 

- https://gitlab.com/wpezsuite/WPezClasses/GoogleAnalyticsGtagJs

- https://gitlab.com/wpezsuite/WPezClasses/GoogleAnalyticsJs

- https://gitlab.com/wpezsuite/WPezClasses/ImageSizeNamesChoose

- https://gitlab.com/wpezsuite/WPezClasses/ImageSizeRegister

- https://gitlab.com/wpezsuite/WPezClasses/ThemeAddSupport

- https://gitlab.com/wpezsuite/WPezClasses/ThemeHeadCleanup



### FAQ

__1 - Why?__

Changing themes and then having to copy various code snippets from one theme to the next is inefficient and ineffective. It was time to move on.

__2 - This doesn't follow the WP coding standards / naming convention?__

Yup. Don't panic. That's okay. Everything is gonna be alright.

__3 - I'm not a developer, can we hire you?__
 
 Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 


### Helpful Links

- https://gitlab.com/WPezPlugins/wpez-theme-customize



### TODO

- ?


### CHANGE LOG

- v0.0.1 - 26 September 2019
  - INIT - hey! ho! let's go!!!
   